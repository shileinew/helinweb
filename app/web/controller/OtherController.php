<?php
/**
 * Created by PhpStorm.
 * User: Rocky
 * Date: 2020/3/7
 * Time: 8:43
 */

namespace app\web\controller;


class OtherController extends BaseController
{
    public function cosmetology()
    {
        return $this->fetch();
    }

    public function cytable()
    {
        return $this->fetch();
    }

    public function diankebang()
    {
        return $this->fetch();
    }

    public function dianshang()
    {
        return $this->fetch();
    }

    public function dstable()
    {
        return $this->fetch();
    }

    public function fans()
    {
        return $this->fetch();
    }

    public function hotel()
    {
        return $this->fetch();
    }

    public function jdtable()
    {
        return $this->fetch();
    }

    public function mdtable()
    {
        return $this->fetch();
    }

    public function mytable()
    {
        return $this->fetch();
    }

    public function one()
    {
        return $this->fetch();
    }

    public function Online()
    {
        return $this->fetch();
    }

    public function restaurant()
    {
        return $this->fetch();
    }

    public function three()
    {
        return $this->fetch();
    }

    public function two()
    {
        return $this->fetch();
    }

    public function website()
    {
        return $this->fetch();
    }

    public function wztable()
    {
        return $this->fetch();
    }
}