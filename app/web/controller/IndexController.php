<?php

/**
 * Created by PhpStorm.
 * User: Rocky
 * Date: 2020/3/6
 * Time: 21:00
 */

namespace app\web\controller;

class IndexController extends BaseController
{
    /**
     * 网站首页
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }
}