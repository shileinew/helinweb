<?php
/**
 * Created by PhpStorm.
 * User: Rocky
 * Date: 2020/3/6
 * Time: 21:02
 */

namespace app\web\controller;

use think\App;
use think\Controller;

class BaseController extends Controller
{
    public function __construct(App $app = null)
    {
        parent::__construct($app);
    }


}