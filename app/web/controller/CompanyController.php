<?php
/**
 * Created by PhpStorm.
 * User: Rocky
 * Date: 2020/3/7
 * Time: 8:22
 */

namespace app\web\controller;


class CompanyController extends BaseController
{
    public function about()
    {
        return $this->fetch();
    }
}