<?php
/**
 * Created by PhpStorm.
 * User: Rocky
 * Date: 2020/3/6
 * Time: 21:07
 */

use think\facade\Route;

//WEB IndexController
Route::rule('', 'web/Index/index');

//WEB UserController
Route::rule('login', 'web/User/login');
Route::rule('apply', 'web/User/apply');

//WEB CompanyController
Route::rule('about', 'web/Company/about');

//WEB OtherController
Route::rule('cosmetology', 'web/Other/cosmetology');
Route::rule('cytable', 'web/Other/cytable');
Route::rule('diankebang', 'web/Other/diankebang');
Route::rule('dianshang', 'web/Other/dianshang');
Route::rule('dstable', 'web/Other/dstable');
Route::rule('fans', 'web/Other/fans');
Route::rule('hotel', 'web/Other/hotel');
Route::rule('jdtable', 'web/Other/jdtable');
Route::rule('mdtable', 'web/Other/mdtable');
Route::rule('mytable', 'web/Other/mytable');
Route::rule('one', 'web/Other/one');
Route::rule('Online', 'web/Other/Online');
Route::rule('restaurant', 'web/Other/restaurant');
Route::rule('three', 'web/Other/three');
Route::rule('two', 'web/Other/two');
Route::rule('website', 'web/Other/website');
Route::rule('wztable', 'web/Other/wztable');