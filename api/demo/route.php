<?php

use think\facade\Route;

Route::resource('demo/articles', 'demo/Articles');
Route::rule('addProvince', 'demo/Articles/addProvince');
Route::rule('addArea', 'demo/Articles/addArea');
Route::rule('addAreaLimit', 'demo/Articles/addAreaLimit');
