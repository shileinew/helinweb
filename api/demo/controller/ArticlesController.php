<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: pl125 <xskjs888@163.com>
// +----------------------------------------------------------------------

namespace api\demo\controller;

use api\common\model\CmfJdAreaModel;
use cmf\controller\RestBaseController;
use think\Cache;

class ArticlesController extends RestBaseController
{
    public function index()
    {
        $token = $this->getToken();
        $provinceUrl = 'https://bizapi.jd.com/api/area/getProvince';
        $cityUrl = 'https://bizapi.jd.com/api/area/getCity';
        $countryUrl = 'https://bizapi.jd.com/api/area/getCounty';
        $townUrl = 'https://bizapi.jd.com/api/area/getTown';
        $result = $this->request_post( $provinceUrl, [
            'token'     => $token,
        ] );
        $result = json_decode($result,true);
        var_dump($result['result']);
    }

    public function addProvince()
    {
        $token = $this->getToken();
        $provinceUrl = 'https://bizapi.jd.com/api/area/getProvince';
        $result = $this->request_post( $provinceUrl, ['token' => $token] );
        $result = json_decode($result,true);
        $areaArr = $result['result'];
        $jdAreaModel = new CmfJdAreaModel();
        $errorNum = 0;
        foreach ($areaArr as $name => $jdAreaId){
            $data = [
                'name' => $name,
                'jd_area_id' => $jdAreaId,
                'level' => 1,
                'parent_id' => 0,
                'path' => $jdAreaId,
            ];
            $res = $jdAreaModel->addInfo($data);
            if(!$res){
                $errorNum++;
            }
        }
        echo $errorNum;
    }

    public function addArea()
    {
        $token = $this->getToken();
        $jdAreaModel = new CmfJdAreaModel();
        $level = 2;
        if($level == 1){
            $url = 'https://bizapi.jd.com/api/area/getCity';
        }else if($level == 2){
            $url = 'https://bizapi.jd.com/api/area/getCounty';
        }else if($level == 3){
            $url = 'https://bizapi.jd.com/api/area/getTown';
        }else{
            echo -1;
        }
        $list = $jdAreaModel->getAreaListByLevel($level);
        $errorNum = 0;
        $total = 0;
        foreach ($list as $key => $val){
            $result = $this->request_post( $url, [
                'token' => $token,
                'id' => $val['jd_area_id'],
            ] );
            $result = json_decode($result,true);
            $areaArr = $result['result'];
            if(!empty($areaArr)){
                $total+=count($areaArr);
                foreach ($areaArr as $name => $jdAreaId){
                    $data = [
                        'name' => $name,
                        'jd_area_id' => $jdAreaId,
                        'level' => $level+1,
                        'parent_id' => $val['jd_area_id'],
                        'path' => $val['path'].";".$jdAreaId,
                    ];
                    $res = $jdAreaModel->addInfo($data);
                    if(!$res){
                        $errorNum++;
                    }
                }
            }
        }
        echo $total."\r\n";
        echo $errorNum;
    }

    public function addAreaLimit()
    {
        $token = $this->getToken();
        $jdAreaModel = new CmfJdAreaModel();
        $level = 3;
        if($level == 1){
            $url = 'https://bizapi.jd.com/api/area/getCity';
        }else if($level == 2){
            $url = 'https://bizapi.jd.com/api/area/getCounty';
        }else if($level == 3){
            $url = 'https://bizapi.jd.com/api/area/getTown';
        }else{
            echo -1;
        }
        $list = $jdAreaModel->getAreaListByGroup();
        $errorNum = 0;
        $total = 0;
        foreach ($list as $key => $val){
            $result = $this->request_post( $url, [
                'token' => $token,
                'id' => $val['jd_area_id'],
            ] );
            $result = json_decode($result,true);
            $areaArr = $result['result'];
            if(!empty($areaArr)){
                $total+=count($areaArr);
                foreach ($areaArr as $name => $jdAreaId){
                    $data = [
                        'name' => $name,
                        'jd_area_id' => $jdAreaId,
                        'level' => $level+1,
                        'parent_id' => $val['jd_area_id'],
                        'path' => $val['path'].";".$jdAreaId,
                    ];
                    $res = $jdAreaModel->addInfo($data);
                    if(!$res){
                        $errorNum++;
                    }
                }
            }
        }
        echo $total."\r\n";
        echo $errorNum;
    }

    public function getToken()
    {
        $cacheKey = 'jd.token';
        $in_redis = \think\facade\Cache::get($cacheKey);

        if ( empty( $in_redis ) ) {

            $now = date( 'Y-m-d H:i:s' );
            $client_id = 'pFq7KKYwMjBkc7C9wIuM';
            $client_secret = 'W5yARsDuP9Kds5c8Ektd';
            $grant_type = 'access_token';
            $username = '中道云科技VOP';
            $password = md5( 'jd123456' );

            $param = [
                'grant_type' => $grant_type,
                'client_id'  => $client_id,
                'username'   => $username,
                'password'   => $password,
                'timestamp'  => $now,
                'sign'       => strtoupper( md5(
                    $client_secret .
                    $now .
                    $client_id .
                    $username .
                    $password .
                    $grant_type .
                    $client_secret
                ) )
            ];

            $response = $this->request_post( 'https://bizapi.jd.com/oauth2/accessToken', $param );
            $data = json_decode( $response, true );
            if ( isset( $data['success'] ) && $data['success'] ) {
                $token_info = $data['result'];
                $token = $token_info['access_token'];
                \think\facade\Cache::set($cacheKey,$token,$token_info['expires_in'] - 60);
//                echo "token 获取成功\n";
                return $token;
            } else {
                exit( "token 获取失败\n" );
            }
        }
//        echo "token 从缓存中获取成功\n";
        return $in_redis;
    }

    public function request_post($url = '', $param = '')
    {
        if (empty($url) || empty($param)) {
            return false;
        }
        $postUrl = $url;
        $curlPost = $param;
        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl); //抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0); //设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1); //post提交方式
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //不验证证书下同
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);
        $data = curl_exec($ch); //运行curl
        curl_close($ch);
        return $data;
    }

}
