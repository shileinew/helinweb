<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/11/19
 * Time: 20:40
 */

namespace api\common\model;

use think\Model;

class CmfJdAreaModel extends Model
{
    protected $table = 'cmf_jd_area';

    public function addInfo($data)
    {
        return $this->insert($data);
    }

    public function getAreaListByLevel($level)
    {
        return $this->where(['level' => $level])->select();
    }

    public function getAreaListByGroup()
    {
        return $this->where("id >= 5665 AND id <= 5666 AND level = 3")->select();
    }

}